const express = require('express');
const router = express.Router();
const User = require('../models/user');

const getUser = (req, res) => {
  User.find((err, people) => {
    if (err) return res.status(500).send(err);
    return res.status(200).send(people);
  });
};

const postUser = ({ body: { firstName, lastName, email, password } }, res) => {
  const newUser = new User({
    firstName,
    lastName,
    email,
    password,
  });
  newUser.save(err => {
    if (err) return res.status(500).send(err);
    return res.status(200).send(newUser);
  });
};

router.get('/', getUser);
router.post('/', postUser);

module.exports = router;
