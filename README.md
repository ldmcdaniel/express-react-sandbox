# express-react-sandbox

## A sandbox to play with react and express.

### To Setup the App:

1. `git clone https://gitlab.com/ldmcdaniel/express-react-sandbox.git`
2. `npm install`

### To Run the App:

1. `npm run dev`
