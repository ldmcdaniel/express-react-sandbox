import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './components/home';
import Login from './components/login';
import Register from './components/register';
import FourOhFour from './components/four-oh-four';
import './App.css';

const App = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/login" component={Login} />
    <Route path="/register" component={Register} />
    <Route component={FourOhFour} />
  </Switch>
);

export default App;
