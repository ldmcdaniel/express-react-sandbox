import React from 'react'; // eslint-disable-line

const Login = props => (
  <>
    <h1>Login</h1>
    <form action="/api/user" method="post">
      <div>
        <label htmlFor="email">Email:</label>
        <input id="email" type="text" />
      </div>
      <div>
        <label htmlFor="password">Password:</label>
        <input id="password" type="password" />
      </div>
      <button>Submit</button>
    </form>
  </>
);
export default Login;
