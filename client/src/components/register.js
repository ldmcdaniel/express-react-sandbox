import React from 'react'; // eslint-disable-line

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
    };
  }

  setFirstName = ({ target: { value: firstName } }) => {
    this.setState({ firstName });
  };

  setLastName = ({ target: { value: lastName } }) => {
    this.setState({ lastName });
  };

  setEmail = ({ target: { value: email } }) => {
    this.setState({ email });
  };

  setPassword = ({ target: { value: password } }) => {
    this.setState({ password });
  };

  submitNewUserRegistration = evt => {
    evt.preventDefault();
    const { firstName, lastName, email, password } = this.state;
    fetch('/api/user/register', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        password,
      }),
    });
  };

  render() {
    const {
      setFirstName,
      setLastName,
      setEmail,
      setPassword,
      submitNewUserRegistration,
    } = this;
    return (
      <>
        <h1>Register</h1>
        <form onSubmit={submitNewUserRegistration}>
          <div>
            <label htmlFor="firstName">First Name:</label>
            <input id="firstName" type="text" onChange={setFirstName} />
          </div>
          <div>
            <label htmlFor="lastName">Last Name:</label>
            <input id="lastName" type="text" onChange={setLastName} />
          </div>
          <div>
            <label htmlFor="email">Email:</label>
            <input id="email" type="text" onChange={setEmail} />
          </div>
          <div>
            <label htmlFor="password">Password:</label>
            <input id="password" type="password" onChange={setPassword} />
          </div>
          <button type="submit">Register</button>
        </form>
      </>
    );
  }
}

export default Register;
