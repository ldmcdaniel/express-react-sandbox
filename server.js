require('dotenv').config();
const express = require('express');
const path = require('path');
const app = express();
const port = process.env.PORT || 5000;
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const User = require('./server/routes/user');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'client/public')));

app.use('/api/user', User);
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/client/public/index.html'));
});

//Start server
app.listen(port, (req, res) => {
  console.log(`Listening on port: ${port}`);
});

mongoose
  .connect(process.env.DB, {
    useNewUrlParser: true,
  })
  .then(() => console.log('successfully connected to mongo'))
  .catch(err => console.log(`Error connecting to mongoose: ${err}`));
